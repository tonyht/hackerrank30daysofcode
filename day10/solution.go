package main

import (
	"math"
	"strconv"
)

func resolve(input string) int {
	n, _ := strconv.ParseInt(input, 10, 64)

	bin := strconv.FormatInt(n, 2)
	return findMax(bin)
}

func findMax(bin string) int {
	max := 0
	counter := 0
	for _, s := range bin {
		if s == '1' {
			counter++
			max = int(math.Max(float64(max), float64(counter)))
		} else {
			counter = 0
		}
	}

	return max
}
