# Hackerrank 30 days of code

[![coverage report](https://gitlab.com/phuonghuynh-eng/hackerrank30daysofcode/badges/master/coverage.svg)](https://gitlab.com/phuonghuynh-eng/hackerrank30daysofcode/commits/master)

This repository is for learning/practicing **Golang** via [`30 days of code`](https://www.hackerrank.com/domains/tutorials/30-days-of-code) challenge.

## How to join us?

Feel free to add folders like `/day10-yourname` and put your code. The code should follow the pattern


- `main.go`: your implementation
- `main_test.go`: testing, it would be better if it contains assertions from Hackerrank.

You can add any packages to `/common` if you want, but please keep all in tested.
